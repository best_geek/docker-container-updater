# Docker image updater

### Why? 
So it occured to me the other day that plenty of people run docker images for months on end. In my own personal cirucmstances, I also noticed that the contents of these images would also be getting missed by vuln scanners. 

99% of the time keeping up to date can minimize so many issues. Most of my build files involve using the package manager to update as the first thing, so I could just tear down the container and rebuild ... but I would like to keep services up as much as possible. Plus, this could just make more issues if I can't rebuild sensibly in an automated fashion. Instead, lets just update them on the fly! 

### The magic
The main magic comes from `update_all_dockers.sh`. This will cater for containers build on Debian, Alpine and CentOS. 

The script does a few things:

* Checks docker is installed and present 

* Enumerates running containers (we will only update those)

* Gets the release information from `/etc/os-release`

* Determines the update command to run within the container


Feel free to modify the `return_pkg_mgr` function to your needs. For example, if you have arch containers, you can modify the if statement to look a bit like this:
```
elif (echo $1 | grep -q "Arch")
        then 
            UPDATE_CMD="pacman ...." 

      else
```

If the script cannot find a package manager it can work with, it will skip that container. 

While bash does not require indented if statements, I have done so for good practice as there is a lot of branching in this script. 

***Easteregg time***

You may notice that `return_pkg_mgr` will populate `UPDATE_CMD` and `UPGRADE_CMD` if it's a Ubuntu/Debian system. 

*Why?* 

Well, for some reason I couldn't update in a one liner for those containers *(ie, apt update && apt upgrade)*. I tried multiple combinations to get this working in the bash script. I think as those commands are present on the docker host, bash tries to escape them in a funny way which when passed to docker fails, as extra quotes get added. 

### Runtime

The `update_all_dockers.sh` can just  be run from the cli on an individual host. 

**BUT WHAT IF I WANT TO RUN IT ON MULTIPLE HOSTS AT THE SAME TIME USING SOMETHING COOL LIKE ANSIBLE?**

This is what `update_dockers.yaml` is for! 

It does the following:

* checks if docker is actually running

* Copies the  `update_all_dockers.sh` shell script to those hosts with docker running

* Executes the shell script

It's worth noting that this playbook can take time and look like it's hanging. That's because the script is going through each container on the host one by one. It will depend on how many containers you have running and what updates they need to pull and install. (First run with 20 outdated containers took a good 8 minutes. Once updated we were done in 2! )

If you're running this, just make sure that the playbook and shell script are in the same folder and run 
`ansible-playbook update_dockers.yaml`

#### Other notes
This was sort of hacked together but quite useful. 

Think about what visibilty you actually have of your containers (are you running vulnerable versions of services)

Feel free to modify and use
