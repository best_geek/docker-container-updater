#!/bin/bash


function return_pkg_mgr {
	UPDATE_CMD=""
	UPGRADE_CMD=""

	#we have to add two possible cmds
	#apt does not play nice with stringed cmds passed thru docker
	#ie, exec ubuntu apt upgrade -y && apt update -y

	if (echo $1 | grep -q "Ubuntu\|Debian")
	then
                UPDATE_CMD="apt update -y"
		UPGRADE_CMD="apt upgrade -y"

	elif (echo $1 | grep -q "Alpine")
	then
                UPDATE_CMD="apk update && apk upgrade"
		
	
	elif (echo $1 | grep -q "CentOS")
        then 
		UPDATE_CMD="yum update -y && yum-upgrade -y" 

	else
		echo "[WARN] Could not determine a package manager"
		UPDATE_CMD="Null" 
	fi

}



#basic docker check
IS_PRESENT=$(which docker)
if [ -z $IS_PRESENT ] 
then
	echo "[ERROR] Docker is not present on this sytem"
	exit
fi

#populate current dockrers
echo "[INFO] Currently running dockers:"
RUNNING=$(docker ps --format '{{.Names}}'  --filter "status=running")
echo "$RUNNING"


echo " "
echo "[INFO] Running through each to determine version..."


#loop thru each container that is running
echo "$RUNNING" | while read CONT
do
	echo " "
	echo "[INFO] [CONTAINER] $CONT"
        docker exec $CONT cat /etc/os-release > /dev/null


	#if we can't find a release file, previous command does not exit cleanly (!=0)
	if [ $? -ne 0 ]
	then
		echo "[WARNING] Linux version for the container: '$CONT' could not be determined. Is it Linux based?"
	else
		OSRELEASE=$(docker exec $CONT cat /etc/os-release | grep "PRETTY_NAME")
		echo �"[INFO] Version: $OSRELEASE"
		
		#determine what package manager to use, populate UPDATE_CMD
		return_pkg_mgr $OSRELEASE


		#couldn't find a pkg manager
		if (echo $UPDATE_CMD | grep -q "Null") 
		then
			echo "[ERROR] Package manager could not be identified for: $CONT  .... skipping"
		
		#go ahead and run
		else
			echo "[INFO] $CONT : Running update" 


			#run our update in the container
 			docker exec  ${CONT} ${UPDATE_CMD}

			#check if a seperate upgrade cmd is specified
			#this is the only work around for apt and command stringing to run two cmmds
			if [ -n "$UPGRADE_CMD" ]
			then
				docker exec  ${CONT} ${UPGRADE_CMD}
			fi
			
			echo "[INFO] $CONT : Finished" 
		fi
	fi


done
